#!/usr/bin/python

####
##  Checks system time vs NTP - @AgentSixty6 - 4/26/2017
##  Uses the ntplib, time, and datetime modules
####

import ntplib
import time
import datetime
import emoji

def doit():
    # set NTP server here
    ntpsvr = "us.pool.ntp.org"
    ## emojis ##
    good = emoji.emojize(':sign_of_the_horns:   :nerd_face:   :sign_of_the_horns:')
    bad = emoji.emojize(':fire:   :fire:   :fire:   :fire:   :fire:   :fire:')
    ############
    qntp = ntplib.NTPClient()
    # NTP request parameters
    query = qntp.request(ntpsvr, version=3)
    # NTP offset query
    offset = query.offset
    # current system time
    ctime = (time.time())
    # diiference of system time and NTP
    offsetcalc = ctime - offset
    # take offsetcalc and convert to human decipherable format
    hoffval = datetime.datetime.fromtimestamp(offsetcalc)
    # clock formatting
    hofftime = (hoffval.strftime("%H:%M:%S"))
    # take ctime and convert to human decipherable format
    hctval = datetime.datetime.fromtimestamp(ctime)
    # clock formatting
    hctime = (hctval.strftime("%H:%M:%S"))
    if offset > 5:
        print bad
        print "Time is not valid | System: %s and NTP: %s, offset of %f" % (hctime, hofftime, offset)
        print bad
        print ("\n" * 2)
    elif offset < -5:
        print bad
        print "Time is not valid | System: %s and NTP: %s, offset of %f" % (hctime, hofftime, offset) 
        print bad
        print ("\n" * 2)
    else:
        print good
        print "Time is valid | System: %s vs NTP: %s, offset of %f" % (hctime, hofftime, offset)
        print good
        print ("\n" * 2)

try:
    while True:
        doit()
        time.sleep(5)
except KeyboardInterrupt:
    exit(0)
